package tests;

import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.List;

import helper.JsonLoader;
import helper.Review;
import list.LinkedList;
import list.ListUtils;
import util.Util;

public class TestLinkedList {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		try {
			
			List<Review> reviews = JsonLoader.getAmazonReviews();
			
			LinkedList<Review> linkedList = new LinkedList<>();
			
			for(Review review : reviews){
				linkedList.insert(review);
			}
			
			System.out.println(Util.getMemoryUsage());
			
			System.out.println(linkedList.getFirst().getSummary());
			System.out.println(linkedList.getLast().getSummary());
			
			LinkedList<Integer> simpleInt = new LinkedList<>();
			
			
			Runnable task = new Runnable() {
				
				@Override
				public void run() {
					long startTime = System.currentTimeMillis();
					
					LinkedList<Review> sortedReviews = (LinkedList<Review>) ListUtils.mergeSort(linkedList);
					
					long endTime = System.currentTimeMillis();
					for(Review review:sortedReviews){
						System.out.println(review.getReviewTime() + "  " + review.getSummary());
					}
					
					System.out.println("Running time: " + (endTime - startTime) + "ms");
					System.out.println(Util.getMemoryUsage());
				}
			};
			
			Thread thread = new Thread(null,task,"ExtraStackThread", 1024*1024 * 2);
			
			thread.start();
			thread.join();
			
			simpleInt.insert(2);
			simpleInt.insert(20);
			simpleInt.insert(10);
			simpleInt.insert(1);
			LinkedList<Integer> sortedList = (LinkedList<Integer>) ListUtils.mergeSort(simpleInt);
			Iterator<Integer> iter = sortedList.iterator();
			
			while(iter.hasNext()){
				System.out.println(iter.next());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
