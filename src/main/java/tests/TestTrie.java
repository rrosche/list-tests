package tests;

import java.util.List;

import tree.Trie;

public class TestTrie {

	public static void main(String[] args) {
		Trie trie = new Trie();
		
		trie.insert("Pizza");
		trie.insert("Pie");
		trie.insert("Skateboard");
		trie.insert("Pepperoni");
		trie.insert("Pastries");
		
		List<String> words = trie.complete("P");
		List<String> words2 = trie.complete("s");
		List<String> words3 = trie.complete("h");
		
		for(String word: words){
			System.out.println(word);
		}
		
		System.out.println("\nSecond Test--");
		
		for(String word: words2){
			System.out.println(word);
		}
		
		System.out.println("\nThird Test--");
		System.out.println(words3.isEmpty()?"No words found":"Something went wrong");
	}

}
