package helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

public class JsonLoader {
	public static List<Review> getAmazonReviews() throws FileNotFoundException{
		Gson gson = new GsonBuilder().setDateFormat("MM dd, yyyy").create();
		JsonReader reader = new JsonReader(new FileReader(new File(ClassLoader.getSystemResource("Amazon_Instant_Video_5.json").getFile())));
		
		Type OBJECT_TYPE = new TypeToken<List<Review>>(){}.getType();
		return gson.fromJson(reader,OBJECT_TYPE);
	}
}
